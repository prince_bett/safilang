# Hello World

```safi
# This is a comment and is ignored by the compiler.
# The file extension for safi is `.sf`

std :: @import("std");

main :: () {
    std.print("Hello World !");
}

```

# Comments

Comments are denoted by `#`.

- `#` : line comment.
- `#[ ... ]#` : block comment.
- `#|` : doc comment.
- `#!` : unix script.

# Types in Safi

The basic types are:

- `num` : number.
- `str` : string.
- `rune` : unicode character.
- `bool` : boolean.
- `null` : is present.

The compound types are:

- `[]`  : record.
- `[?]` : union (enum).
- `()`  : closure.

# Literals

- numbers: `42i32` `0.3f64` `132` `3.142`
- character: `'a'`
- string: `"Hello"`
- binary: `0b0101`
- octal : `0o7171`
- hexadecimal: `0xff`

> underscores can be used to separate numbers, binary,
> octal and hexadecimal e.g `1_000_000`, `0xFF_FF_FF`

# Operators

- constant

# Keywords.

`num` `str`
`rune` `bool` `true` `false`
`if` `else` `match` `for` `in`
`break` `continue` `while` `as`
`defer` `null` `async` `await`

## Casting

TODO
