# Introduction

Safi lang intends to be a fast and delightful scripting language
for writing plugins and configuration files. The language is
designed to be heavily extendable. It is inspired by lua where the
only complex data structures are:
- records (lua tables).
- ?? enums (tables with only one field initialised).

The syntax if inspired by jai, rust and zig.

The primary targets are wasm and lua.

## Features

- Statically typed.
- Delightful error messages.
- Compile time metaprogramming. (jai/zig).
- Full type inference.

## Documentation.

- [Core language](./core.md)
